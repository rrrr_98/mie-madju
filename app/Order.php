<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = array(
    'user_id',
    'id_barang',
    'jumlah'
  );

}
