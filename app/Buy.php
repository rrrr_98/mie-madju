<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
  protected $fillable = array(
    'user_id',
    'id_barang',
    'jumlah'
  );

}
